//inport thư viên express js tương đương import "express" từ express
const express = require("express");

//khởi tạo 1 app express
const app = express();

//khai báo cổng chạy project
const port = 8000;

//call back function là một function đóng vai trò là 1 tham số của 1 function khác, nó sẽ được thưucj hiện khi fuction chủ được gọi
//khai báo API dạng /
app.get("/", (req, res)=>{
let today = new Date();

    res.json({
        message : `xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()+1} năm ${today.getFullYear()}`
    })
})

app.listen(port, () => {
    console.log("app listening on port : " , port);
})